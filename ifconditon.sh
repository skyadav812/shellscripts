#!/bin/bash
#Author Sunil Kumar
clear
#if [ condition ]
#then
#  statement
#fi

#number=10
if [ $1 -eq 10 ]
then
echo "First Argument is Ten"
fi

#num=15
if [[ $2 -ne 13 ]]
then 
echo "Second argument is not equal to 13"
fi

echo "======================================================="
#  if [[ condition ]]
#   then
#    statement
#   else
#    statement
#  fi
#count=35
if [[ $3 -le 30 ]]
 then
  echo "Third argument is less then and equal to 30"
 else
  echo "Third argument is greater than to 30"
fi

echo "=============if elif then else========================"
# if [[ condition ]]
# then 
#   statement
# elif [[ condition ]]
#  then
#    statement
#   else
#    statement
# fi

if [[ $4 == "sunil" ]]
 then
  echo "You are Sunil"
 elif [[ $4 == "ishant" ]]
  then
   echo "You are Ishant"
 else
   echo "You are not Sunil or Ishant"
fi

