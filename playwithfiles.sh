#!/bin/bash
#this script is used for find a file details
#written by Sunil Yadav
#Date of Modified: 17 Oct 2021
echo -e "Please enter file name which one you want to find details: \c"
read file_name

if [[ -f $file_name ]]
 then 
  echo "The $file_name is found"
    if [[ -s $file_name ]]
    then 
     echo "The $file_name is not Empty"
    else
     echo "The $file_name is Empty"
    fi
echo "Now we check file permissions"
  if [[ -r $file_name ]]
   then
   echo "In this file $file_name has read permissions"
  else
   echo "In this file $file_name has no read permissions"
  fi
else
  echo "The $file_name is not exist"
fi
#####
#-e	file exists	[[ -e /path/to/file ]]
#-f	file is a regular file, /
#not a directory	[[ -f /path/to/file ]]
#-d	File is a directory	[[ -d /path/to/directory ]]
#-s	File is not zero size	[[ -s /path/to/file ]]
#-L	File is symbolic link	[[ -L /path/to/file ]]
#-b	File is a block device	[[ -b /path/to/file ]]
#-p	File is a pipe device	[[ -p /path/to/file ]]
#-S	File is a socket	[[ -S /path/to/file ]]
#-r	File has read permission for user.	[[ -r /path/to/file ]]
#-w	File has write permission for user.	[[ -w /path/to/file ]]
