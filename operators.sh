#!/bin/bash
#written by Sunil Kumar 
#First Operator is AND or && or -a
echo -e "Please enter you experince as linux admin: \c"
read linx_exp
#First way to use AND operator
if [ "$linx_exp" -ge 3 ] && [ "$linx_exp" -le 8 ]
 then 
  echo "You are valid user for this course"
else
  echo "You are not valid user for this course"
fi
#Second way to use AND operator
if [ "$linx_exp" -ge 3 -a "$linx_exp" -le 8 ]
 then
  echo "You are valid user for this course"
else
  echo "You are not valid user for this course"
fi
#Third way to use AND operator
if [[ "$linx_exp" -ge 3 && "$linx_exp" -le 8 ]]
 then
  echo "You are valid user for this course"
else
  echo "You are not valid user for this course"
fi

# Second Operatior most commanly used OR or II or -o
echo -e "Please enter you experince as linux admin again: \c"
read linx_expag
#First way to use OR operator
if [ "$linx_expag" -gt 25 ] || [ "$linx_expag" -lt 8 ]
 then
  echo "You are valid user for this course"
else
  echo "You are not valid user for this course"
fi
#Second way to use AND operator
if [ "$linx_expag" -gt 25 -o "$linx_expag" -lt 8 ]
 then
  echo "You are valid user for this course"
else
  echo "You are not valid user for this course"
fi
#Third way to use AND operator
if [[ "$linx_expag" -gt 25 || "$linx_expag" -lt 8 ]]
 then
  echo "You are valid user for this course"
else
  echo "You are not valid user for this course"
fi

