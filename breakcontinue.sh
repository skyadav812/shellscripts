#!/bin/bash
#break and continue statments in shell scripting
for (( i=1; i<=10; i++ ))
do
 echo "$i"
done
echo "===================Break for loop in between==========="
for (( i=1; i<=10; i++ ))
do
  if [ $i -gt 5 ]
   then
    break
  fi
 echo "$i"
done
echo "================Continue with skip some number=========="
for (( i=1; i<=10; i++ ))
do
  if [ $i -eq 3 -o $i -eq 6 -o $i -eq 9 ]
   then
    continue
  fi
 echo "$i"
done
