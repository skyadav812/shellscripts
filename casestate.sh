#!/bin/bash
vehicle=$1

case $vehicle in

"car" )
 echo "Rent of $vehicle is 50 rupees" ;;
"van" )
 echo "Rent of $vehicle is 40 rupees" ;;
"bicycle" )
 echo "Rest of $vehicle is 10 rupees" ;;
"bus" )
 echo "Rent of $vehicle is 5 rupees" ;;
* )
 echo "This vehicle is not avaialble for rent"
 echo "So, please try to use car, van, bicycle or bus" ;;
esac
