#!/bin/bash
n=1
#while [ $n -le 5 ]
# do
#   echo -e "Please Enter a Number: \c"
#   read number
#   n=$(( n+1 ))
# done

while (( $n <= 2 ))
 do
  echo -e "Please Enter a Number: \c"
  read number
  (( n++ ))
 done


#now read data from a file using while loop
#first create a file with userlist.txt
echo "User1" > userlist.txt
echo "User2" >> userlist.txt
echo "User3" >> userlist.txt
echo "User4" >> userlist.txt
echo "User5" >> userlist.txt

while read username
  do
    echo "Username is: $username"
    sleep 2
  done < userlist.txt

#Now read user's password file from linux machine
while read userlist
  do
#    useris=`$userlist | awk -F ":" '{print $1}'`
    echo "$userlist"
    sleep 1
  done < /etc/passwd
