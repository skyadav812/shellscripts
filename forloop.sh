#!/bin/bash
#for loop scripts basic
for i in 1 2 3 4 5
do
 echo $i
done
#we can print 1 to 10 with below example
echo "==========================================="
for i in {1..10}
do
 echo $i
done
#we can print 1 to 10 with below example
echo "==========================================="
for i in {2..50..2}
do
 echo $i
done
#we can print 1 to 10 with below example
echo "==========================================="
for (( i=1; i<=5; i++ ))
do 
  echo $i
done
#we can print 1 to 10 with below example
echo "=========run some command using for loop==="
for command in ls pwd date cal
do
 echo "=================$command================="
 $command
done

#we can print all directory in current dir.
echo "=======Directories in current dir================="
for item in *
 do
  if [ -d $item ]
 then
  echo $item
 fi
done

#we can print all directory in current dir.
echo "=======Files in current dir================="
for item in *
 do
  if [ -f $item ]
 then
  echo $item
 fi
done
#we can print all directory in current dir.
echo "=======Files & Dir in current dir================="
for item in *
 do
  if [ -e $item ]
 then
  echo $item
 fi
done
