#!/bin/bash
#select is used for list
#select name in mark john tom roky
#do
#echo "$name selected"
#done
clear
echo "Please select any number from below list as per required"
select command in memory cpuinfo meminfo loadavg
do
 case $command in
   memory)
    free -h
    ;;
   cpuinfo)
    cat /proc/cpuinfo | grep cores
    ;;
   meminfo)
    cat /proc/meminfo | head -5
    ;;
   loadavg)
    uptime
    ;;
   *)
    echo "Error: Please provide the no. between 1 to 4 "
    ;;
   esac
done





